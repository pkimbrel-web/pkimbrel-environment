resource "aws_acm_certificate" "certificate" {
  domain_name       = "${var.app_subdomain}.staging.${var.domain}"
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "cert_validation_record" {
  name    = aws_acm_certificate.certificate.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.certificate.domain_validation_options.0.resource_record_type
  zone_id = var.zone_id
  records = [aws_acm_certificate.certificate.domain_validation_options.0.resource_record_value]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "cert_validation" {
  certificate_arn         = aws_acm_certificate.certificate.arn
  validation_record_fqdns = [aws_route53_record.cert_validation_record.fqdn]
}







resource "aws_acm_certificate" "certificate_active" {
  domain_name       = "${var.app_subdomain}.${var.domain}"
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "cert_validation_record_active" {
  name    = aws_acm_certificate.certificate_active.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.certificate_active.domain_validation_options.0.resource_record_type
  zone_id = var.zone_id
  records = [aws_acm_certificate.certificate_active.domain_validation_options.0.resource_record_value]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "cert_validation_active" {
  certificate_arn         = aws_acm_certificate.certificate_active.arn
  validation_record_fqdns = [aws_route53_record.cert_validation_record_active.fqdn]
}
