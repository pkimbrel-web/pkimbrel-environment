resource "aws_secretsmanager_secret" "okta_secret" {
  name = "okta-client-secret"
}

resource "aws_secretsmanager_secret_version" "okta_secret_version" {
  secret_id     = aws_secretsmanager_secret.okta_secret.id
  secret_string = <<EOF
{
  "client_id": "${okta_app_oauth.okta_auth.client_id}",
  "client_secret": "${okta_app_oauth.okta_auth.client_secret}",
}
EOF
}
