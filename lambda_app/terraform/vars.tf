variable "account" {}
variable "region" {}
variable "env" {}
variable "domain" {}
variable "app_subdomain" {}
variable "zone_id" {}
variable "okta_url" {}
variable "authorizer_version" {}
