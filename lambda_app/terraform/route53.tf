resource "aws_api_gateway_domain_name" "app_domain_gateway" {
  certificate_arn = aws_acm_certificate_validation.cert_validation.certificate_arn
  domain_name     = "${var.app_subdomain}.staging.${var.domain}"
  security_policy = "TLS_1_2"
}

resource "aws_route53_record" "app_domain_record" {
  zone_id = var.zone_id
  name    = "${var.app_subdomain}.staging.${var.domain}"
  type    = "A"

  alias {
    name                   = aws_api_gateway_domain_name.app_domain_gateway.cloudfront_domain_name
    zone_id                = aws_api_gateway_domain_name.app_domain_gateway.cloudfront_zone_id
    evaluate_target_health = false
  }
}

resource "aws_api_gateway_domain_name" "app_domain_gateway_active" {
  certificate_arn = aws_acm_certificate_validation.cert_validation_active.certificate_arn
  domain_name     = "${var.app_subdomain}.${var.domain}"
  security_policy = "TLS_1_2"
}

resource "aws_route53_record" "app_domain_record_active" {
  zone_id = var.zone_id
  name    = "${var.app_subdomain}.${var.domain}"
  type    = "A"

  alias {
    name                   = aws_api_gateway_domain_name.app_domain_gateway_active.cloudfront_domain_name
    zone_id                = aws_api_gateway_domain_name.app_domain_gateway_active.cloudfront_zone_id
    evaluate_target_health = false
  }
}
