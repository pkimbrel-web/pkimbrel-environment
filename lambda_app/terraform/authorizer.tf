resource "aws_iam_role" "lambda_role" {
  name = "lambda-app-authorizer-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "cloudwatch_policy" {
  name   = "lambda-app-authorizer-role-policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [{
    "Sid": "CloudWatchCreate",
    "Action": [
      "logs:CreateLogGroup"
    ],
    "Resource": "arn:aws:logs:${var.region}:${var.account}:*",
    "Effect": "Allow"
  },
  {
    "Sid": "CloudWatchPutEvents",
    "Action": [
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ],
    "Resource": "arn:aws:logs:${var.region}:${var.account}:log-group:/aws/lambda/lambda-app-authorizer:*",
    "Effect": "Allow"
  }]
}
EOF
}

resource "aws_iam_role_policy_attachment" "cloudwatch_policy_attachment" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.cloudwatch_policy.arn
}

resource "aws_cloudwatch_log_group" "lambda_log_group" {
  name              = "/aws/lambda/lambda-app-authorizer"
  retention_in_days = 30
}

resource "aws_lambda_function" "lambda" {
  filename      = "../lambda_app/upload/authorizer-${var.authorizer_version}.zip"
  function_name = "lambda-app-authorizer"
  role          = aws_iam_role.lambda_role.arn
  handler       = "index.handler"
  timeout       = 30
  memory_size   = 1024

  runtime = "nodejs12.x"


  environment {
    variables = {
      OKTA_URL = var.okta_url
    }
  }
}

