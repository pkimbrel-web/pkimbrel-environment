resource "okta_app_oauth" "okta_auth" {
  label = "Lambda App"
  type  = "web"
  #omit_secret    = true
  response_types = ["code"]
  grant_types = [
    "authorization_code",
    "refresh_token"
  ]
  redirect_uris = [
    "https://oauth.pstmn.io/v1/callback",
    "http://localhost:8081/authorization-code/callback"
  ]
  lifecycle {
    ignore_changes = [users, groups]
  }
}

output "client_id" {
  value = okta_app_oauth.okta_auth.client_id
}
