import OktaJwtVerifier from '@okta/jwt-verifier'

//https://dev-5220749.okta.com
const oktaJwtVerifier = new OktaJwtVerifier({
  issuer: `${process.env.OKTA_URL}/oauth2/default`
})

exports.handler = function(event) {
  return new Promise((resolve, _reject) => {
    const methodArn = event.methodArn.split(":")
    const accountId = methodArn[4]

    oktaJwtVerifier.verifyAccessToken(event.authorizationToken, "api://default")
    .then(jwt => {
      resolve({
        principalId: accountId,
        policyDocument: {
          "Version": "2012-10-17",
          "Statement": [{
            "Action": "execute-api:Invoke",
            "Effect": "Allow",
            "Resource": event.methodArn
          }]
        },
        context: {
          ...jwt?.claims
        }
      })
    }).catch(_err => {
      console.log(_err)
      resolve({
        principalId: accountId,
        policyDocument: {
          "Version": "2012-10-17",
          "Statement": [{
            "Action": "execute-api:Invoke",
            "Effect": "Deny",
            "Resource": event.methodArn
          }]
        }
      })
    })
  })
}