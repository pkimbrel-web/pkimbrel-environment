resource "aws_route53_zone" "main" {
  name = "dev.paulkimbrel.com"
}

variable "AUTHORIZER_VERSION" {}

module "lambda_app_module" {
  source             = "../lambda_app/terraform"
  account            = "747613746747"
  region             = "us-east-1"
  env                = "dev"
  domain             = "dev.paulkimbrel.com"
  app_subdomain      = "lambda-app"
  zone_id            = aws_route53_zone.main.id
  okta_url           = "https://dev-5220749.okta.com"
  authorizer_version = var.AUTHORIZER_VERSION
}

output "client_id" {
  value = module.lambda_app_module.client_id
}
