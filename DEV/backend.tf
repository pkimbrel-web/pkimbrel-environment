provider "aws" {
  region  = "us-east-1"
  profile = "personal-dev"
  version = "~> 2.0"
}

provider "okta" {
  org_name = "dev-5220749"
  base_url = "okta.com"
}

terraform {
  backend "s3" {
    region  = "us-east-1"
    bucket  = "com.paulkimbrel.dev.terraform"
    key     = "backends/us-east-1/core"
    profile = "personal-dev"
  }
}

